export const capitalize = (string) => {
    if (typeof string !== 'string') {
        return ''
    }

    return string.charAt(0).toUpperCase() + string.slice(1)
}

export const getRandomFloatBetween = (min, max) => {
    return Math.random() * (max - min) + min;
}

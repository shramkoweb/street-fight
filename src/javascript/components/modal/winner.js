import { showModal } from './modal';

export function showWinnerModal(fighter) {
    // call showModal function
    const { name: title, } = fighter;
    const bodyElement = document.createElement('p')
    bodyElement.innerText = `${title} is win`

    showModal({ title, bodyElement });
}

import { createElement } from '../helpers/domHelper';
import { capitalize } from '../helpers/util';

function createFighterInfo(fighter) {
  const { source, _id, ...otherInfo } = fighter;
  const listElement = createElement({ tagName: 'ul', attributes: {style: 'list-style: none'}});

  for (const [key, value] of Object.entries(otherInfo)) {
    const listItemElement = createElement({tagName: 'li'})
    listItemElement.innerText = `${capitalize(key)} - ${value}`;

    listElement.appendChild(listItemElement)
  }

  return listElement;
}

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if (fighter) {
    const { source, name } = fighter;
    const textStyle = "color: white; font-weight: bold; margin: 0;";
    fighterElement.setAttribute('style', textStyle);
    const imageElement = createElement({
      tagName: 'img',
      attributes: {
        src: source,
        title: name,
        alt: name,maxHeight: '400px'
    }});

    fighterElement.appendChild(imageElement);
    fighterElement.appendChild(createFighterInfo(fighter));
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

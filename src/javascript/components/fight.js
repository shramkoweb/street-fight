import { controls } from '../../constants/controls';
import { getRandomFloatBetween } from '../helpers/util';
import throtle from 'lodash/throttle'

export async function fight(firstFighter, secondFighter) {
    return new Promise((resolve) => {
        // resolve the promise with the winner when fight is over
        const arena = document.querySelector('.arena___root');
        const helthbar = arena.querySelectorAll('.arena___health-bar')[1]
        const helthbar2 = arena.querySelectorAll('.arena___health-bar')[0]
        const firstStreetFighter = getFighter(firstFighter);
        const secondStreetFighter = getFighter(secondFighter);
        let keysPressed = {};


        // Listen to attack is PRESSED
        document.addEventListener('keyup', (evt) => {
            if (firstStreetFighter.currentHealth <= 0) {
                resolve(secondStreetFighter)
            } else if(secondStreetFighter.currentHealth <= 0) {
                resolve(firstStreetFighter);
            }
            keysPressed = {};

            switch (evt.code) {
                case controls.PlayerOneAttack:
                    toAttack(firstStreetFighter, secondStreetFighter, helthbar);
                    break;
                case controls.PlayerTwoAttack:
                    toAttack(secondStreetFighter, firstStreetFighter, helthbar2)
                    break;
                case controls.PlayerOneBlock:
                    firstStreetFighter.isBlocking = false;
                    break;
                case controls.PlayerTwoBlock:
                    secondStreetFighter.isBlocking = false;
                    break;
                default:
                    break;
            }
        })

        // TODO REFACTOR
        const throttledOne = throtle(() => toCriticalAttack(firstStreetFighter, secondStreetFighter, helthbar), 5000, { 'trailing': false });
        const throttledSecond = throtle(() => toCriticalAttack(secondStreetFighter, firstStreetFighter, helthbar2), 5000, { 'trailing': false });

        // Listen to block  & critical attack is PRESS now
        document.addEventListener('keydown', (evt) => {
            if (firstStreetFighter.currentHealth <= 0) {
                resolve(secondStreetFighter)
            } else if(secondStreetFighter.currentHealth <= 0) {
                resolve(firstStreetFighter);
            }
            keysPressed[evt.code] = true;

            // TODO REFACTOR ?
            if (controls.PlayerOneCriticalHitCombination.every((code) => keysPressed.hasOwnProperty(code))) {
                throttledOne()
            } else if (controls.PlayerTwoCriticalHitCombination.every((code) => keysPressed.hasOwnProperty(code))) {
                throttledSecond()
            }

            switch (evt.code) {
                case controls.PlayerOneBlock:
                    firstStreetFighter.isBlocking = true;
                    break;
                case controls.PlayerTwoBlock:
                    secondStreetFighter.isBlocking = true;
                    break;
                default:
                    break;
            }

        })
    });
}

function toAttack(attacker, defender, healthBar) {
    if (defender.isBlocking || attacker.isBlocking) {
        return;
    }

    defender.currentHealth -= getDamage(attacker, defender);

    updateHealthBar(healthBar, defender);
}

function toCriticalAttack(attacker, defender, healthBar) {
    defender.currentHealth -= attacker.attack * 2;

    updateHealthBar(healthBar, defender);
}

function updateHealthBar(healthBar, fighter) {
    const { health, currentHealth } = fighter;
    const healthInPercents = (currentHealth * 100) / health;
    const toolbarWidth = healthInPercents > 0 ? healthInPercents : 0;

    healthBar.style.width = `${toolbarWidth}%`;
}

function getFighter(fighter) {
    return {
        ...fighter,
        currentHealth: fighter.health,
    };
}

export function getDamage(attacker, defender) {
    const damage = getHitPower(attacker) - getBlockPower(defender);

    return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
    const { attack } = fighter;
    const criticalHitChance = getRandomFloatBetween(1, 2);

    return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
    const { defense } = fighter;
    const dodgeChance = getRandomFloatBetween(1, 2);

    return defense * dodgeChance;
}
